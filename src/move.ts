export type List = Folder[];

export type Folder = {
  id: string;
  name: string;
  files: File[];
};

export type File = {
  id: string;
  name: string;
};

export default function move(list: List, source: string, destination: string): List {
  const folder = list.filter((_Files) => _Files.files.filter((_File) => _File.id === source)[0])[0]; // Filtered folder by source id
  if (folder) {
    const file = folder.files.filter((_File) => _File.id === source)[0]; // Filtered file by source id
    const destinationFolder = list.find((_Folder) => _Folder.id === destination); // Detected destination folder by id
    if (destinationFolder) {
      destinationFolder.files.push(file); // Source file pushed to destination folder
      folder.files = folder.files.filter((_File) => _File.id !== source); // Source file deleted to source folder
      return list;
    }
    throw new Error('You cannot specify a file as the destination');
  } else {
    throw new Error('You cannot move a folder');
  }
}
